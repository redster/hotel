import React from 'react';

import './NavBarComponent.css';
import logo from '../NavBarComponent/src/logo.svg'
import arrow from '../NavBarComponent/src/arrow.svg'
import arrow1 from '../NavBarComponent/src/arrow.svg'


const NavBarComponent = () => {
    return (
        <div className="c-main-navbar">
            <div className="c-main-navbar_wrapper">
                <div className="c-main-navbar_logo"><img src={logo} alt="logo"/></div>
                <ul className="c-main-navbar_menu">
                    <li className="c-main-navbar_menu_item">О нас</li>
                    <li className="c-main-navbar_menu_item">Услуги</li><img className="arrow" src={arrow1} alt="arrow"/>
                    <li className="c-main-navbar_menu_item">Вакансии</li>
                    <li className="c-main-navbar_menu_item">Новости</li>
                    <li className="c-main-navbar_menu_item">Соглашение</li><img className="arrow" src={arrow} alt="arrow"/>
                </ul>
                <div className="c-main-navbar_buttons">
                    <button className="c-main-navbar_button-enter">Войти</button>
                    <button className="c-main-navbar_button-registration">Зарегистрироваться</button>
                </div>
            </div>
        </div>
    );
}

export default NavBarComponent