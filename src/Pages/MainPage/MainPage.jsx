import React from 'react';

import './MainPage.css';
import background from '../MainPage/src/background.png';

//import custom components
import NavBar from '../../CommonComponents/NavBarComponent/NavBarComponent';
import Registration from '../../CommonComponents/RegistrationComponent/Registration';
const MainPage = () => {
    return (
        <div>
            <NavBar/>
            <div className="images"><img classname="background" src={background} alt="background"/></div>
            <Registration/>
        </div>
    )
}

export default MainPage;